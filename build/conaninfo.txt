[settings]
    arch=x86_64
    build_type=Release
    compiler=gcc
    compiler.libcxx=libstdc++
    compiler.version=7
    os=Linux

[requires]
    eigen/3.Y.Z

[options]


[full_settings]
    arch=x86_64
    arch_build=x86_64
    build_type=Release
    compiler=gcc
    compiler.libcxx=libstdc++
    compiler.version=7
    os=Linux
    os_build=Linux

[full_requires]
    eigen/3.3.7@conan/stable:5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9

[full_options]


[recipe_hash]


[env]

